# kubernetes-meetup

## Requirements

- Google Cloud SDK
- Kubectl

## Contribution guide

### Remotes

The **remotes** follow the convention:

- _**origin**_: fork in the account of the developer

- _**upstream**_: main repository

### Commit Style

Please you consider de following git styles for commit messages:

http://udacity.github.io/git-styleguide/

## License

MIT

**Free Software, Hell Yeah!**
